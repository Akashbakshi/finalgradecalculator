function CalculateGradeNeeded(currentGrade,desiredGrade,finalWeigth){
    return Number(desiredGrade)+Number((desiredGrade-currentGrade)*(100.0-finalWeigth))/finalWeigth; // Formula to calculate final grade is (Goal − Current × (100% − Final Weight)) / Final Weight
}

function DetermineGrade(){
    var grade = document.getElementById('currentGrade').value;
    var desiredGrade = document.getElementById('desiredGrade').value;
    var weight = document.getElementById('examWeight').value;

    var gradeNeeded = CalculateGradeNeeded(grade,desiredGrade,weight);
    
    var message = "";

    if(gradeNeeded < 0)
        message = ", Congrats you don't even have to take the exam";
    else if (gradeNeeded < 30 && gradeNeeded >= 0)
        message = ", You're in the clear! Just write anything on the exam.";
    else if(gradeNeeded < 50 && gradeNeeded >= 30)
        message = ", You're cutting it close, but you can do it !";
    else if(gradeNeeded < 75 && gradeNeeded >= 50)
        message = ", Go and start studying, you'll need all the knowledge you can get";
    else if(gradeNeeded < 100 && gradeNeeded >= 75)
        message = ", You'll need a miracle to pass";

    alert("You would need a "+gradeNeeded+"% on your final exam to get at least a "+desiredGrade+"%"+message);

}