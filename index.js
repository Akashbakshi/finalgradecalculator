
var express = require('express');
var app = express();

app.use(express.static(__dirname + "/public/"));

app.listen(3000,function(){
    console.log("Server started at port 3000");
    
});

app.get('/',function(req,res){
    res.sendFile(__dirname+"/views/index.html");
});
